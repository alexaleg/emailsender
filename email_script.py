# !/email_script.py
# This script is used to send emails automathically with or without attachments

import smtplib
from email.mime.multipart import MIMEMultipart
from email import Encoders
from email.MIMEBase import MIMEBase
from email.mime.text import MIMEText
from email.header import Header
import codecs
import sys
reload(sys)
sys.setdefaultencoding("utf-8")

# Sending email function
def email_send(usrnm, pwd, sender, receiver, message, subject, filepath, filename ):

    # Log in to the account
    smtpserver = smtplib.SMTP("smtp.gmail.com",587)
    smtpserver.ehlo()
    smtpserver.starttls()
    smtpserver.ehlo
    smtpserver.login(usrnm,pwd)

    # Message construction
    msg = MIMEMultipart()
    msg['Subject'] = Header(subject,'utf-8')
    msg['From'] = sender
    msg['To'] = receiver

    # Body of the message
    msg.attach(MIMEText(message, 'plain','utf-8'))

    # Attaching document
    part = MIMEBase('application', "octet-stream")
    part.set_payload(open(filepath, "rb").read())
    Encoders.encode_base64(part)

    # Include header for the attached file
    part.add_header('Content-Disposition', 'attachment', filename=filename)
    msg.attach(part)

    # Send message
    try:
       smtpserver.sendmail(sender, receiver, msg.as_string())
       print "Successfully sent email"
    except Exception:
       print "Error: unable to send email"

# Test email
def email_testmail():
    user = ''
    pwd = ''
    sender = ""
    receiver = ""

    name = 'Test name'

    subject = "Test subject"
    filepath = "test path"
    filename = "test file"
    #email_send(user,pwd,sender, receiver, message, subject, filepath, filename)

if __name__ == '__main__':
    email_testmail()
