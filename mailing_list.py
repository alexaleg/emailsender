# -*- coding: utf-8 -*-
# This script allows to send emails with personalized messages
# to a set of recipients in a .xlsx-.xls file
import xlrd
import sys
from email_script import *
import codecs
reload(sys)
sys.setdefaultencoding("utf-8")

# Open the file
workbook = xlrd.open_workbook('')
worksheet = workbook.sheet_by_index(1) # Select the sheet from where the file is going to be taken

# Set up email credentials and attachments
user = ''
pwd = ''
sender = ''
subject = ''

# Get file to be attached
filepath = ""
filename = ""

 # Open the main part of the message, constant
    msg = codecs.open('', 'r', 'utf-8')
    message = msg.read()

# Begin to send emails
for i in range(23,70):
    failed = 0
    name = worksheet.cell(i, 4).value
    address = worksheet.cell(i, 6).value

    if name  == xlrd.empty_cell.value or address == xlrd.empty_cell.value:
        failed += 1
        print "No hay dirección de correo"
    else:
        # Set up personalized message
        message = " %s \n\n%s" %(nombre,message)
        email_send(user,pwd,sender, address, message, subject, filepath, filename)
        print "Email sent to %s" %nombre
    # if n == 3:
        # break # if there are 3 blank spaces, stop the algorithm
